#!/bin/bash

# leadIn.sh
# created by: Travis Llado, travis@travisllado.com
# last edit: 2017.07.02

# A simple script that 
# - creates a lead-in line at the beginning of a print to prevent gaps in the 
#   raft outline, and 
# - ensures that the extruder heaters are not turned off prematurely.

################################################################################

# Assuming that extruder priming begins with the first "G92" ...
primeLine="$(cat $1 | grep G92 -n -m 1 | cut -f1 -d:)"
# Assuming that extrusion begins with the first "G0" ...
extrudeCmd=$(cat $1 | grep G0 -m 1)
# Assuming that shutdown sequence starts with the last "M107" ...
shutdownLine="$(cat $1 | grep M107 -n | tail -1 | cut -f1 -d:)"

# Create commands for raft lead-in
startX="$(echo $extrudeCmd | cut -d " " -f 3 | cut -c 2-)"
startY="$(echo $extrudeCmd | cut -d " " -f 4 | cut -c 2-)"
hypot="$(echo "sqrt($startX * $startX + $startY * $startY)/10 + 4.5" | bc -l)"
leadInLength="$(echo $hypot | cut -d "." -f 1 | cut -c -3)"
leadInLength=$leadInLength".""$(echo $hypot | cut -d "." -f 2 | cut -c -2)"
leadInCmd="G1 X""$startX"" Y""$startY"" E""$leadInLength"

# Copy original file + add-ins to new file
echo ";This file was edited by Travis Llado's Cura post-processor" > $1"2"
echo ";Blame him if it doesn't work (travis@travisllado.com)" >> $1"2"
echo ";" >> $1"2"
sed -n "1,$(expr $primeLine)p" $1 >> $1"2"
echo "G0 X50 Y5 Z0.36" >> $1"2"
echo "G1 F1125 X5 Y5 E4.5" >> $1"2"
echo $leadInCmd >> $1"2"
sed -n "$(expr $primeLine + 2),$(expr $shutdownLine - 1)p" $1 >> $1"2"
echo "M400" >> $1"2"
sed -n "${shutdownLine},\$p" $1 >> $1"2"

# Replace original file with new file
mv $1"2" $1

################################################################################
# Dev Notes

# 2017.07.01
# Added in second section that inserts "M400" at beginning of shutdown sequence.
# This prevents the printer from turning off the extruder heaters before the
# print is completed.

# 2017.07.02
# Changed script so that instead of assuming pertinent data's line number in 
# file, instead assumes its location with respect to program.

# 2017.08.05
# Added in lines that add my name to the beginning of every file because I'm
# vain. Also realized instead of running this from Terminal I can just paste it 
# into an Automator app then drag/drop gcode files onto it. This saves literally 
# seconds of my time.